import json
import os
import spacy

parser = spacy.load("en_core_web_md")

with open('data/cleaned_entities.json') as f:
    clean_entities = json.load(f)

