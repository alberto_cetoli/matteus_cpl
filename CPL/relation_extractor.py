from tools import spacy_mapper_str
from tools import create_entitiy_dict, create_relation_dict
from crowdsource import ranks


relation_range = 10


def get_verb_relation(ents):
    triples = [find_verb_relation(ent) for ent in ents]
    return triples


def find_verb_relation(ent):
    chunk = [ent.start, ent.end]
    case = ent.root
    triple=()
    verb_relations = [i for i in case.ancestors if i.pos_ =="VERB"]# and abs(case.i-i.i) < relation_range ]
    if not verb_relations:
        verb_relations = [i for i in case.children if i.pos_=="VERB"]# and abs(case.i-i.i) < relation_range ]
    if verb_relations:
        for verb in verb_relations:
            direction = verb.i-case.i
            direction = direction/abs(direction)
            known_ents_children = [i for i in verb.children if i.ent_iob_ !="O" and i.i not in range(chunk[0],chunk[1])
                                   and ((verb.i-i.i)/abs(verb.i-i.i)) != direction]
            if known_ents_children:
                ent_sec = get_NER_chunk(known_ents_children[0])
                triple = get_triple(ent, ent_sec)
                if triple:
                    break
            else:
                known_ents_ancestors = [i for i in verb.ancestors if
                                       i.ent_iob_ != "O" and i.i not in range(chunk[0], chunk[1])
                                        and (verb.i - i.i) / abs(verb.i - i.i) is not direction]
                if known_ents_ancestors:
                    ent_sec = get_NER_chunk(known_ents_ancestors[0])
                    triple = get_triple(ent, ent_sec) # here should the CPL be done!!!!
                    if triple:
                        break
        if not triple:
            etype = spacy_mapper_str(ent.label_)
            if etype:
                triple = get_single_ent_context_triple(ent,etype)
    else:
        etype = spacy_mapper_str(ent.label_)
        if etype:
            triple = get_single_ent_context_triple(ent, etype)
    return triple


def get_NER_chunk(ient):
    return [i for i in ient.doc.ents if ient.i in range(i.start, i.end)][0]


def get_NER_chunk_old(ent):
    i_count = ent.i; doc = ent.doc
    doc_len = len(doc)
    i_ent = i_count - 1
    j_ent = i_count + 1
    while i_ent > 0:
        if doc[i_ent].ent_iob_ == "O":
            i_ent += 1
            break
        i_ent -= 1
    while j_ent < doc_len:
        if doc[j_ent].ent_iob_ == "O":
            break
        j_ent += 1
    return doc[i_ent:j_ent]


def get_spacy_relation(ents):
    doc = ents[0].doc
    ent_i = [(ent.start,ent.end) for ent in ents]
    all_rel = [(doc[ent_i[inum][1]+1:item[0]].text, ents[inum].text, ents[inum+1].text)
               for inum, item in enumerate(ent_i[1:]) if item[0]-ent_i[inum][1] < 8]
    return all_rel


def is_useful_context(context):
    is_useful = True
    return is_useful


def get_triple(ent, ent_sec):
    i_ent = ent.start; j_ent = ent_sec.start
    ent_sec_type = spacy_mapper_str(ent_sec[0].ent_type_)
    ent_type = spacy_mapper_str(ent[0].ent_type_)
    triple = ()
    if ent_type and ent_sec_type:
        doc = ent.doc
        if i_ent > j_ent:
            ent1_surf = ent_sec.text; ent2_surf= ent.text
            relation_chunk = doc[j_ent:ent.end]
            ent1_type = ent_sec_type
            ent2_type = ent_type
        else:
            ent1_surf = ent.text; ent2_surf= ent_sec.text
            relation_chunk = doc[i_ent:ent_sec.end]
            ent1_type = ent_type
            ent2_type = ent_sec_type
        # here is where I will feed in the surface_form of a relation and get back a list of possible relation types and entities associated with them
        # surface_form = relation_chuck.text
        # ['rel_type','ent1','ent2', conf] = a_func(surface_form)
        triple = (create_entitiy_dict(surface_form=ent1_surf, etype=ent1_type,
                                      confidence=ranks.beliefs['spacy_belief'], source="spacy"),
                  create_relation_dict(surface_form=relation_chunk.text,
                                       rtype=ent1_type+"_"+ent2_type, confidence=1, source="verb_relation"),
                  create_entitiy_dict(surface_form=ent2_surf, etype=ent2_type,
                                      confidence=ranks.beliefs['spacy_belief'], source="spacy"))
    elif ent_type:
        triple = get_single_ent_context_triple(ent, ent_type)

    return triple


def get_entity_context(ent):
    context = ent.sent
    return context


def get_single_ent_context_triple(ent, label=False):
    if not label:
        label = spacy_mapper_str(ent.label_)
    context = get_entity_context(ent)
    # here is where I will feed in the surface_form of a relation and get back a list of possible relation types and entities associated with them
    # surface_form = context.text
    # ['rel_type','ent1','ent2', conf] = a_func(surface_form)
    triple = (main_manger.get_topic_entity(),
              create_relation_dict(surface_form=context.text,
                                   rtype="page_"+label, confidence=1, source="context"),
              create_entitiy_dict(surface_form=ent.text, etype=label,
                                  confidence=ranks.beliefs['spacy_belief'], source="spacy"))
    return triple


def filter_ents(ents):
    #might need to remove this function
    return [ent for ent in ents if ent.text.strip(" ")]
