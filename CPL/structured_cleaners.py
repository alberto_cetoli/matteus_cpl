from utils import parser, clean_entities#ß, alberto_ner
from tools import isName, flatten, find_tokens, spacy_mapper, create_entitiy_dict, create_relation_dict, clean_text
from crowdsource import ranks
import numpy as np
from nameparser import HumanName

odd_chars=np.array([" (", " -", "- ",", ", " ,", " / "," -->", " +",":"])
re_odd_chars=np.array([" \(", " -", "- ",", ", " ,", " / "," -->", " \+",":"])


classes= ['skill','company','academy','location','person','role','sector','artifact']


def create_cleaned_dicts(entity, text_list, c_confidence, etype=False, rtype="is_cleaned"):
    if not etype:
        etype = entity["type"]
    prior_confidence = entity["confidence"]
    entities = [create_entitiy_dict(surface_form=i, etype=etype,
                                            confidence=prior_confidence + (1 - prior_confidence) * c_conf,
                                            source='cleaner') for i, c_conf in zip(text_list, c_confidence)]
    triples = [(entity, create_relation_dict(rtype=rtype, surface_form=rtype, source="cleaner",
                                             confidence=1),
                i) for i in entities]
    return entities, triples


def get_cleaned_entity(entity):
    entities =[]; triples=[]
    text = entity["surface_form"]; etype=entity["type"]
    text = clean_text(text)
    text_list = chunking_odd_text(text, etype)
    if text_list:
        confidence = []
        for text in text_list:
            if is_in_clean_list(text, etype):
                confidence.append(ranks.beliefs['in_clean_list_confidence'])
            else:
                confidence.append(0)
        if etype is "person":
            entities, triples = create_cleaned_dicts(entity, text_list, confidence, rtype="alias")
        else:
            entities, triples = create_cleaned_dicts(entity, text_list, confidence)
    else:
        if etype is "academy":
            text = strip_punct_digits(text)
        if is_in_clean_list(text, etype):
            entities, triples = create_cleaned_dicts(entity, [text], [ranks.beliefs['in_clean_list_confidence']])
    return entities, triples


def is_in_clean_list(entity, etype):
    if etype in classes:
        return entity in clean_entities[etype]
    return False


def verify_spacy_ner(entity, label):
    return any([entity in clean_entities[i] if i and (i in clean_entities) else False for i in spacy_mapper(label)])


def strip_punct_digits(text):
    p_text = parser(text)
    out_text = ""
    for item in p_text:
        if not item.is_punct and not item.is_digit:
            out_text += item.text_with_ws
    out_text = out_text.strip(" ")
    if not out_text:
        out_text = text
    return out_text


def are_odd_chars(text):
    olist = np.array([True if i in text else False for i in odd_chars])
    if olist.any():
        return True
    return False


def noun_neighbours(tok):
    child_neighbours = [i for i in tok.children if abs(i.i - tok.i) == 1]
    nouns = [noun_neighbours(i) for i in child_neighbours if i.pos_ in ["NOUN", "PROPN"]]
    nouns = flatten(nouns)
    if nouns is []:
        nouns.extend([i for i in tok.head.children if abs(i.i-tok.i) == 1 and i.pos_ in ["NOUN", "PROPN"]])
    nouns.append(tok)
    return nouns


def find_instance_context(tok):
    proper_context = tok.text
    if tok.pos_ in ["NOUN","PROPN"]:
        chunk = [i.i for i in noun_neighbours(tok)]
        if len(chunk) < 2:
            context = [[j for j in i.children if j.pos_ in ["NOUN","PROPN"]] for i in tok.children if i.text == "of"]
            if context != []:
                if context[0] != []:
                    proper_context += " of "+context[0][0].text
        else:
            chunk.sort()
            proper_context = tok.doc[chunk[0]:chunk[-1]+1].text
    return proper_context


def find_chunk_instance(text,instance):
    p_text = parser(text)
    matches = [i.text for i in p_text.noun_chunks if i.text == instance or i.root.text == instance]
    if not matches:
        matches = [i for i in p_text if i.text == instance and i.pos_ in ["PROPN","NOUN"]]
        if not matches:
            matches = [i.text for i in find_tokens(p_text,instance)]
        else:
            matches = [find_instance_context(i) for i in matches]
    return matches


def person_parser(text):
    hname = HumanName(text)
    text_list =[]
    if isName(hname.last):
        text_list = [hname[i] for i in hname._members if hname[i] != '']
        text_list.append(hname.full_name)
    return text_list


def chunking_odd_text(text,etype):
    text_list =[]
    is_odd = are_odd_chars(text)
    if is_odd and etype is "person":
      text_list = person_parser(text)
    elif is_odd:
        cleaned_list = [find_chunk_instance(text, i) for i in clean_entities[etype] if i in text]
        if cleaned_list != []:
            text_list = list(set(flatten(cleaned_list)))
    return text_list



domains = ['about.me', 'angellist.', 'crunchbase.', 'delicious.', 'facebook.', 'flickr.', 'foursquare.', 'friendfeed.', 'github.', 'glassdoor.', 'googleplus.', 'gravatar.', 'klout.', 'lanyrd.', 'last.fm', 'myspace.', 'pinterest.', 'plancast.', 'quora.', 'stackoverflow.', 'twitter.', 'vimeo.', 'youtube.']

def clean_url(entity):
    surface_form = entity["surface_form"]
    match = [i for i in domains if i in surface_form]
    c_entity, triples = [],[]
    if match:
        c_entity, triples = create_cleaned_dicts(entity, [surface_form], [0.8], etype="url_"+match[0].rstrip("."), rtype="is_superclass")
    return c_entity, triples