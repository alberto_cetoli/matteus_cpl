import json
import numpy as np
from .extractor import PatternHandler, extract_instances
from .ontology import ontolgyHandler

ontology = ontolgyHandler()
seedfile = 'data/seed.json'
instance_threshold = 2
pattern_threshold = 0.95
pattern_prior=0.5
sigma_0 = 1
sigma = 1
conf_limit = 10
pattern_limit = 3000


def get_posterior_mu(data,mu_0):
    return (mu_0 / sigma_0 ** 2 + sum(data) / sigma ** 2) / (1. / sigma_0 ** 2 + len(data) / sigma ** 2)

class PatternLearner:

    def __init__(self,seedfile=seedfile):
        self.ontology = ontolgyHandler()
        self.Phandler = PatternHandler()
        with open(seedfile,"r") as f:
            seeds = json.load(f)
        self.promoted_patterns =[]
        self.promoted_instances = []
        for seed in seeds:
            self.promoted_patterns.extend([pattern for pattern in self.Phandler.get_patterns_list(seed[0],seed[1],seed[2]) if pattern not in self.promoted_patterns])
            self.promoted_instances.extend([(ins,etype) for ins, etype in zip(seed[1],seed[2])]) # list of (instance, entity_type)
        self.promoted_instances = list(set(self.promoted_instances))
        self.candidate_instances = [] # list of (instance, entity_type)
        self.candidate_patterns=[] # dict of pattern: [entity_types]
        self.candidate_instance_rank = []
        self.promoted_instance_rank = [3]*len(self.promoted_instances)
        self.candidate_pattern_rank = []
        self.promoted_pattern_rank = [pattern_threshold]*len(self.promoted_patterns)
        self.ref_triples=[]

    def extract_instances(self,text):
        ic_pattern = 0
        for pattern, p_types in self.promoted_patterns:
            instances, raw_triple = extract_instances(pattern, text)
            if instances:
                ref_triple = [-1,ic_pattern,-1]
                for instance, etype in zip(instances,p_types):
                    triple_index = raw_triple.index(instance)
                     # get the instance precision
                    co_occuring = [etemp for itemp,etemp in self.candidate_instances if itemp == instance]
                    instance_tuple = (instance, etype)
                    precision = 1
                    if co_occuring:
                        co_occuring.append(etype)
                        precision = co_occuring.count(etype)
                        unique_co = list(set(co_occuring))
                        if len(unique_co)> 1:
                            uni_count = np.array([co_occuring.count(co_item) for co_item in unique_co])
                            uni_idx = uni_count.argsort()
                            if ontology.are_entities_disjoin(unique_co[uni_idx[-1]],unique_co[uni_idx[-2]]):
                                if uni_count[-1]/uni_count[-2] >= 3:
                                    instance_tuple = (instance, unique_co[uni_idx[-1]])
                                    precision = uni_count[-1]
                                else:
                                    precision = 0
                    ref_triple[triple_index] = len(self.candidate_instances)
                    self.candidate_instance_rank.append(precision)
                    self.candidate_instances.append(instance_tuple)
                self.ref_triples.append(tuple(ref_triple))
            ic_pattern +=1

    def add_candidate_patterns(self, pattern_tuple):
        if self.candidate_patterns:
            pats, ptypes = zip(*self.candidate_patterns)
            p_mapped = [self.ontology.entity_pair_mapper(p) for p in ptypes]
            npats, nptypes, np_mapped = np.array(pats), np.array(ptypes), np.array(p_mapped)
            pattern, pat_types = pattern_tuple
            if pattern_tuple in self.candidate_patterns:
                this_patmap = np_mapped[np.where(npats == pattern)]
                n_types = len(this_patmap)
                this_pat_type = self.ontology.entity_pair_mapper(pat_types)
                n_this_type = len(npats[np.where(this_patmap == this_pat_type)])
                idx = self.candidate_patterns.index(pattern_tuple)
                prior_rank = self.candidate_pattern_rank[idx]
                self.candidate_pattern_rank[idx] = get_posterior_mu([n_this_type / n_types], prior_rank)
            else:
                this_patmap = np_mapped[np.where(npats == pattern)]
                self.candidate_patterns.append(pattern_tuple)
                if len(this_patmap)> 0:
                    n_types = len(this_patmap)
                    this_pat_type = self.ontology.entity_pair_mapper(pat_types)
                    n_this_type = len(npats[np.where(this_patmap == this_pat_type)])
                    rank = get_posterior_mu([n_this_type/n_types], 0.5)
                    self.candidate_pattern_rank.append(rank)
                else:
                    self.candidate_pattern_rank.append(pattern_prior)
        else:
            self.candidate_patterns.append(pattern_tuple)
            self.candidate_pattern_rank.append(pattern_prior)


    def extract_patterns(self, text):
        self.Phandler.clear_patterns()
        incl_instances = [(instance,etype) for instance, etype in self.promoted_instances if instance in text]
        if incl_instances:
            instances, etypes = zip(*incl_instances)
            patterns = self.Phandler.get_patterns_list(text, instances, etypes)
            if patterns:
                for pattern_tuple in patterns:
                    self.add_candidate_patterns(pattern_tuple)

    def promote_instance_candidates(self):
        c_i_rank = np.array(self.candidate_instance_rank)
        for ic, promote in enumerate(c_i_rank>instance_threshold):
            if promote:
                if self.candidate_instances[ic] in self.promoted_instances:
                    idx = self.promoted_instances.index(self.candidate_instances[ic])
                    self.promoted_instance_rank[idx] += self.candidate_instance_rank[ic]
                else:
                    self.promoted_instances.append(self.candidate_instances[ic])
                    self.promoted_instance_rank.append(self.candidate_instance_rank[ic])

    def promote_pattern_candidates(self):
        c_p_rank = np.array(self.candidate_pattern_rank)
        deletes = []
        n_prom_pats = len(self.promoted_patterns)
        if n_prom_pats> 1000:
            print("There are %d of promoted patterns! We need to think of a better strategy soon!"%n_prom_pats)
        limit = len(self.candidate_patterns)-pattern_limit
        for ic, promote in enumerate(c_p_rank > pattern_threshold):
            if promote:
                if self.candidate_patterns[ic] in self.promoted_patterns:
                    idx = self.promoted_patterns.index(self.candidate_patterns[ic])
                    pattern_prior = self.promoted_pattern_rank[idx]
                    self.promoted_pattern_rank[idx] = get_posterior_mu(self.candidate_pattern_rank[ic:ic+1],pattern_prior)
                else:
                    if n_prom_pats < pattern_limit:
                        self.promoted_patterns.append(self.candidate_patterns[ic])
                        self.promoted_pattern_rank.append(self.candidate_pattern_rank[ic])
            else:
                if len(deletes) < limit:
                    deletes.append(ic)

        deletes.sort(); deletes.reverse()
        for id in deletes:
            del self.candidate_patterns[id]
            del self.candidate_pattern_rank[id]

    def clear_candidate_instances(self):
        self.candidate_instances = []
        self.candidate_instance_rank =[]
        self.ref_triples =[]

    def get_instance_belief(self):
        return [min(rank,conf_limit)/conf_limit for rank in self.candidate_instance_rank]

