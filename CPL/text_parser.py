from utils import parser, clean_entities#ß, alberto_ner
from tools import spacy_mapper, clean_text, spacy_mapper_str, create_entitiy_dict, create_relation_dict
from CPL.cpl import PL
from crowdsource import ranks
from structured_cleaners import get_cleaned_entity
from relation_extractor import filter_ents, get_verb_relation


# Freetext Extractor
def freetext_extractor(text, get_relation=True):
    all_ents = []
    text_separated = text.split("\n")
    for i_text in text_separated:
        text = clean_text(i_text)
        all_ents.extend(list(parser(text).ents))
    triples =[]
    if all_ents:
        all_ents = filter_ents(all_ents)
        # if we do not want any relations then we should put get_relation = False
        if get_relation:
            # here the triples from the freetext are created, the NLP magic
            triples = get_verb_relation(all_ents)

    # here we sort and clean the spacy entities
    all_ents, cleaned_triples = get_entity_dicts(all_ents)
    triples.extend(cleaned_triples)

    #this is where CPL comes to play
    freetext_cpl_extractor(text)
    cpl_triples, cpl_entities = get_CPL_triples()
    for entity in cpl_entities:
        refined_entities, refined_triples = get_cleaned_entity(entity)
        cpl_entities.extend(refined_entities)
        cpl_triples.extend(refined_triples)
    triples.extend(cpl_triples)
    all_ents.extend(cpl_entities)

    return all_ents, triples


def get_entity_dicts(all_ents):
    # creating the entity dicts and sending to the cleaner
    out_ents = []; cleaned_triples=[]
    for ent in all_ents:
        etype = spacy_mapper_str(ent.label_)
        if etype:
            # Make the entity dict
            entity = create_entitiy_dict(surface_form=ent.text, etype=etype,
                                         confidence=ranks.beliefs["spacy_belief"], source="spacy")
            out_ents.append(entity)

            # sending the entity dict to the cleaner
            if etype in classes:
                refined_entities, triples = get_cleaned_entity(entity)
                out_ents.extend(refined_entities)
                cleaned_triples.extend(triples)
    return out_ents, cleaned_triples


#CPL functions
def freetext_cpl_extractor(text):
    text = clean_text(text)
    PL.extract_instances(text)


def add_CPL_patterns(patterns):
    [PL.add_candidate_patterns(pattern) for pattern in patterns if pattern]


def get_CPL_entities():
    PL.promote_instance_candidates()
    c_belief=PL.get_instance_belief()
    instances = PL.candidate_instances
    PL.clear_candidate_instances()
    return [(instance[0],instance[1], ranks.beliefs['cleaner_verified_belief']+
             (1-ranks.beliefs['cleaner_verified_belief'])*belief) if is_in_clean_list(instance[0],instance[1])
            else (instance[0],instance[1],ranks.beliefs['cpl_belief']+(1-ranks.beliefs['cpl_belief'])*belief)
            for belief, instance in zip(c_belief,instances)]

def get_CPL_triples():
    PL.promote_instance_candidates()
    c_belief = PL.get_instance_belief()
    instances = PL.candidate_instances
    triples = PL.ref_triples
    PL.clear_candidate_instances()
    triple_list = []
    entity_list = [create_entitiy_dict(surface_form=instance[0], etype=instance[1], confidence=conf, source='cpl') for
                   instance, conf in zip(instances, c_belief)]
    for ent1, rel, ent2 in triples:
        if ent2 >= 0:
            t_ent = create_entitiy_dict(surface_form=instances[ent2][0],etype=instances[ent2][1],confidence=c_belief[ent2], source="cpl")
        else:
            t_ent = False
        if ent1 >= 0:
            if t_ent:
                f_ent = create_entitiy_dict(surface_form=instances[ent1][0],etype=instances[ent1][1],confidence=c_belief[ent1],source="cpl")
            else:
                f_ent=main_manger.get_current_page()
                t_ent = create_entitiy_dict(surface_form=instances[ent1][0], etype=instances[ent1][1],
                                            confidence=c_belief[ent1], source="cpl")
        else:
            f_ent = main_manger.get_current_page()
        pattern, p_type = PL.promoted_patterns[rel]
        if len(p_type)>1:
            rtype = p_type[0]+"_"+p_type[1]
            sform_rel = instances[ent1][0] + pattern.strip("__") + instances[ent2][0]
        else:
            rtype = "page_" + p_type[0]
            sform_rel = pattern.replace("__",instances[ent1][0])
        relation = create_relation_dict(surface_form=sform_rel ,rtype=rtype,
                                        confidence=PL.promoted_pattern_rank[rel], source="cpl")
        triple_list.append((f_ent,relation,t_ent))
    return triple_list, entity_list


def promote_cpl_patterns():
    PL.promote_pattern_candidates()


#Cleaners
def verify_spacy_ner(entity, label):
    return any([entity in clean_entities[i] if i else False for i in spacy_mapper(label)])

classes= ['skill','company','academy','location','person','role','sector','artifact']


def is_in_clean_list(entity, etype):
    if etype in classes:
        return entity in clean_entities[etype]
    return False
