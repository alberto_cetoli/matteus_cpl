
class Ranking:
    def __init__(self):
        self.beliefs = {'skill':0.5,'company': 0.5 ,'academy':0.5,'location':0.5,'person':0.6,'role':0.5,'sector':0.5,'artifact':0,
           'cleaner_verified_belief' : 0.7, 'spacy_belief': 0.5, 'cpl_belief': 0.4, 'in_clean_list_confidence': 0.5,
           'structured_belief' : 0.7}

        self.belief_labels = [i for i in self.beliefs]
        self.belief_vector = [self.beliefs[i] for i in self.belief_labels]

    def update_belief(self,delta_rank,label):
        self.beliefs[label] = min(delta_rank+ self.beliefs[label] , 1)


ranks = Ranking()
