#from entities.models import KnownEntity
import re


spacy_labels = ["GPE","LOC","ORG","PERSON","LANGUAGE","WORK_OF_ART","PRODUCT"] #,"WORK_OF_ART","PRODUCT"
spacy_convert = [["location"],["location"],["company","academy"],["person"],["skill"],["artifact"],["artifact"]] #,["product"],["product"]
spacy_convert_str = ["location","location","company","person","skill","artifact","artifact"] # ,"product","product" org

surnames = open('data/CSV_Database_of_Last_Names.csv').read().split('\r')

def isName(input_name):
    if input_name in surnames:
        return True
    else:
        return False


def clean_list_of_entities(entity_list):
    return [i for i in entity_list if not any([True for j in list(set(entity_list)-set([i])) if i in j])]


flatten = lambda l: [item for sublist in l for item in sublist]


def find_tokens(parsed_text, instance):
    instances = [i for i in re.finditer(instance.replace("+","\+"),parsed_text.text_with_ws)]
    tokens_ind = [[j.i for j in parsed_text if j.idx >= i.start() and j.idx+len(j.text) <= i.end()] for i in instances]
    # tokens = [i for i in parsed_text if any([True for j in instances if i.idx >= j.start() and i.idx < j.end() ])]
    chunks = []
    if tokens_ind != []:
        if tokens_ind[0] !=[]:
            chunks = [parsed_text[i[0]:i[-1]+1] for i in tokens_ind if i]
    return chunks


def spacy_mapper(etype):
    if etype in spacy_labels:
        return spacy_convert[spacy_labels.index(etype)]
    return [False]


def spacy_mapper_str(etype):
    if etype in spacy_labels:
        return spacy_convert_str[spacy_labels.index(etype)]
    return etype


def get_chunk(token):
    doc = token.doc
    n_tokens = len(doc)
    i_right, i_left =token.i+1, max(0,token.i-1)
    while i_right < n_tokens:
        if doc[i_right].pos_ not in ["NOUN","PROPN"]:
            if doc[i_right].text !="of":
                if doc[i_right-1].text =="of":
                    i_right -= 1
                break
        i_right+=1
    while i_left != 0:
        if doc[i_left].pos_ not in ["NOUN","PROPN"]:
            if doc[i_left].text !="of":
                if doc[i_left+1].text =="of":
                    i_left += 1
                break
        i_left-= 1
    return doc[i_left:i_right]


def get_sentences(text):
    sentences = text.split("\n")
    rtext = []
    for itext in sentences:
        tlen = len(itext)
        if tlen > 150 and itext.count(". ") > 0: #approx length of a sentance
            rtext.extend(itext.split(". "))
        else:
            rtext.append(itext)
    return rtext


def clean_text(text):
    try:
        text = bytes(text, "ISO-8859-1", "ignore").decode("unicode-escape")
    except:
        pass
    text = re.sub('\s+', ' ', text).strip()
    return text


def create_entitiy_dict(surface_form, etype, confidence, source, convert_spacy=False):
    if etype =="the_page":
        raise NameError
    if convert_spacy:
        c_type =spacy_mapper_str(etype)
        if c_type:
            etype = c_type
    return {"surface_form":surface_form,"type":etype,"confidence":confidence, "source":source }


def create_relation_dict(rtype, source,confidence, surface_form, from_index=False, to_index=False):
    return {'from': from_index, 'to': to_index, 'type': rtype, "source": source, "confidence": confidence, "surface_form":surface_form}

