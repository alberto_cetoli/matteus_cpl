import json
import os
import numpy as np


class ontolgyHandler():
    def __init__(self):
        with open('data/Current_Ontology.js') as f:
            ontology = json.load(f)
        with open('data/disjoints.json') as f:
            reason = json.load(f)
        self.entity_types = ontology["concepts"]
        self.entity_list = [i for i in self.entity_types]
        self.relation_types = ontology["properties"]
        self.relation_list = [i for i in self.relation_types]
        self.n_entities = len(self.entity_list)
        self.n_relations = len(self.relation_list)
        self.entity_exclusion_matrix = np.ones([self.n_entities, self.n_entities])
        for is_disjoint, ent_tup in zip(reason["is_disjoint"], reason["entity_pairs"]):
            self.entity_exclusion_matrix[
                self.entity_list.index(ent_tup[0]), self.entity_list.index(ent_tup[1])] = is_disjoint
            self.entity_exclusion_matrix[
                self.entity_list.index(ent_tup[1]), self.entity_list.index(ent_tup[0])] = is_disjoint
        self.relation_exclusion_matrix = np.ones([self.n_relations, self.n_relations])
        self.entity_pairs = []
        for entity1 in self.entity_list:
            self.entity_pairs.append([entity1])
            for entity2 in self.entity_list:
                self.entity_pairs.append([entity1, entity2])

    def are_entities_disjoin(self, entity1, entity2):
        return bool(self.entity_exclusion_matrix[self.entity_list.index(entity1), self.entity_list.index(entity2)])

    def are_relations_disjoin(self, relation1, relation2):
        return bool(
            self.entity_exclusion_matrix([self.relation_list.index(relation1), self.relation_list.index(relation2)]))

    def entity_pair_mapper(self, entity_pair):
        return self.entity_pairs.index(entity_pair)

    def entity_pair_finder(self, idx):
        return self.entity_pairs[idx]
