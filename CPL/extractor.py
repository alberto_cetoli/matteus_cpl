from .utils import parser
from .tools import find_tokens

BAD_PATTERNS = ["is","was","have","has"]
SUBJECTS = ["nsubj", "nsubjpass", "csubj", "csubjpass", "agent", "expl"]
OBJECTS = ["dobj", "dative", "attr", "oprd","pobj"]


class PatternHandler():

    def __init__(self):
        self.patterns = {}
        self.classes = []


    def class_check(self,etype):
        if etype not in self.classes:
            self.classes.append(etype)

    def get_patterns(self,s, instances, etypes):
        parsed_s = parser(s)
        cases_types = [(item,etype) for ins,etype in zip(instances,etypes) for item in find_tokens(parsed_s,ins)]
        self.i_cases, self.types = zip(*[(i,types) for case, types in cases_types for i in range(case.start,case.end)])
        for case, etype in cases_types:
            pattern, pat_types = self.get_pattern(case, etype)
            self.class_check(etype)
            if pattern:
                self.patterns[pattern] = pat_types

    def get_patterns_list(self, s, instances, etypes):
        patterns_list =[]
        parsed_s = parser(s)
        cases_types = [(item, etype) for ins, etype in zip(instances, etypes) for item in
                       find_tokens(parsed_s, ins)]
        self.i_cases, self.types = zip(
            *[(i, types) for case, types in cases_types for i in range(case.start, case.end)])
        # print(cases_types)
        for case, etype in cases_types:
            pattern, pat_types = self.get_pattern(case, etype)
            # self.class_check(etype)
            if pattern:
                if (pattern,pat_types) not in patterns_list:
                    patterns_list.append((pattern,pat_types))
        return patterns_list

    def get_pattern(self, case, etype):
        chunk_range = list(range(case.start, case.end + 1))
        root = case.root; iterator = root; is_rel = False; all_i = []; incl_end = True; i_iter = iterator.i
        while iterator.head.i != iterator.i:
            iterator = iterator.head
            i_iter = iterator.i
            all_i.append(i_iter)
            if iterator.pos_ in ["NOUN", "PROPN"]:
                if iterator.i in self.i_cases:
                    is_rel = True; incl_end = False; break
                elif iterator.pos_ is "PROPN":
                    incl_end = False; break
        c_child = close_children(iterator, chunk_range)
        if not is_rel:
            while c_child:
                iterator = c_child[0]
                i_iter = iterator.i
                incl_end = True
                all_i.append(i_iter)
                if iterator.pos_ in ["NOUN", "PROPN"]:
                    if iterator.i in self.i_cases:
                        is_rel = True; incl_end = False; break
                    elif iterator.pos_ is "PROPN":
                        incl_end = False; break
                c_child = close_children(iterator, chunk_range)
        if abs(root.i - i_iter) < 3:
            alt_iter = [i for i in all_i if abs(root.i - i) > 2]
            if not alt_iter:
                return False, False
            i_iter = alt_iter[0]
        direction = int((i_iter - root.i) / abs(i_iter - root.i))
        current_instance = max(chunk_range) + 1 if direction == 1 else min(chunk_range) - 1
        case_range = range(current_instance + direction, i_iter + direction, direction)
        mid_instance = [i for i in case_range if i in self.i_cases]
        all_types = [etype]
        if direction == 1:
            if mid_instance: i_iter = min(mid_instance); incl_end = False; is_rel = True
            if abs(i_iter - current_instance + incl_end) < 3: return False, False
            pattern = "__" + case.doc[current_instance - 1].whitespace_ + case.doc[
                                                                          current_instance:i_iter + incl_end].text_with_ws
            if is_rel:
                all_types = [etype, self.types[self.i_cases.index(i_iter)]]
                pattern = pattern + "__"
        else:
            if mid_instance: i_iter = max(mid_instance); incl_end = False; is_rel = True
            if abs(current_instance - i_iter + (not incl_end)) < 3: return False, False
            pattern = case.doc[i_iter + (not incl_end):current_instance + 1].text_with_ws + "__"
            if is_rel:
                all_types = [self.types[self.i_cases.index(i_iter)], etype]
                pattern = "__" + case.doc[i_iter].whitespace_ + pattern
        return pattern, all_types

    def clear_patterns(self):
        self.patterns = {}


def close_children(case,chunk_range):
    return [child for child in case.children if child.i not in chunk_range and abs(child.i-case.i)< 4]


def get_chunk_left(token):
    doc = token.doc
    i_left = token.i-1
    while i_left != 0:
        if doc[i_left].pos_  not in ["NOUN","PROPN"]:
            i_left +=1
            break
        i_left-= 1
    return doc[i_left:token.i+1]

def get_chunk_right(token):
    doc = token.doc
    n_tokens = len(doc)
    i_right=token.i+1
    while i_right < n_tokens:
        if doc[i_right].pos_  not in ["NOUN","PROPN"]:
            break
        i_right+=1
    return doc[token.i:i_right]


def extract_instance_left(pat, text):
    s_index = text.find(pat)
    if s_index == -1:
        return False
    p_text = parser(text)
    instance =[i for i in p_text.noun_chunks if s_index-i.end_char in [0,1]]
    if instance:
        return instance[0].text
    instance =[i for i in p_text if s_index-i.idx in [0,1] and i.pos_ in ["NOUN","PROPN"]]
    if instance:
        return get_chunk_left(instance[0]).text
    return False


def extract_instance_right(pat, text):
    e_index = text.find(pat)
    if e_index == -1:
        return False
    e_index+=len(pat)
    p_text = parser(text)
    instance =[i for i in p_text.noun_chunks if i.start_char-e_index in [0,1]]
    if instance:
        return instance[0].text
    instance =[i for i in p_text if i.idx-e_index in [0,1] and i.pos_ in ["NOUN","PROPN"]]
    if instance:
        return get_chunk_right(instance[0]).text
    return False


def extract_instances(pattern, text):
    p_split = pattern.split("__")
    prev_empty= False; prev_frac=None
    instances = []; triple = [False, False, False]
    for pat_frac in p_split:
        if pat_frac:
            if prev_empty:
                instance = extract_instance_left(pat_frac,text)
                if instance:
                    instances.append(instance)
                    triple[0] = instance; triple[1] = pat_frac
                else:
                    return [],()
            prev_frac = pat_frac
            prev_empty=False
        else:
            if prev_frac:
                instance = extract_instance_right(prev_frac,text)
                if instance:
                    instances.append(instance)
                    triple[1] = prev_frac; triple[2] = instance
                else:
                    return [], ()
            prev_empty=True
    return instances, tuple(triple)

