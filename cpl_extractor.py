from CPL.cpl import PatternLearner

PL = PatternLearner()

text = 'Microsoft was acquired by Apple'

if __name__ == '__main__':
    PL.extract_instances(text)
    PL.extract_patterns(text)

    print(PL.candidate_instances)
    print(PL.candidate_patterns)

