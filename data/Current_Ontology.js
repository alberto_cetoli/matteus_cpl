{
	"properties": {
		"owns": {
			"range": [
				"artifact"
			],
			"name": "owns",
			"domain": [
				"company"
			],
			"description": "<add description>",
			"ident": 56
		},
		"states": {
			"range": [
				"reference"
			],
			"name": "states",
			"domain": [
				"person"
			],
			"description": "<add description>",
			"ident": 41
		},
		"utilizes": {
			"range": [
				"skill"
			],
			"name": "utilizes",
			"domain": [
				"artifact"
			],
			"description": "<add description>",
			"ident": 55
		},
		"makes": {
			"range": [
				"investment"
			],
			"name": "makes",
			"domain": [
				"person"
			],
			"description": "<add description>",
			"ident": 49
		},
		"contributes_to": {
			"range": [
				"artifact"
			],
			"name": "contributes_to",
			"domain": [
				"username"
			],
			"description": "<add description>",
			"ident": 54
		},
		"advises": {
			"range": [
				"company"
			],
			"name": "advises",
			"domain": [
				"person"
			],
			"description": "<add description>",
			"ident": 47
		},
		"worked_at": {
			"range": [
				"company"
			],
			"name": "worked_at",
			"domain": [
				"person"
			],
			"description": "<add description>",
			"ident": 35
		},
		"has_property": {
			"range": [
				"tagline",
				"time_frame",
				"timestamp",
				"description",
				"achievements",
				"mission",
				"size",
				"type",
				"person"
			],
			"name": "has_property",
			"domain": [
				"person",
				"role",
				"degree",
				"company",
				"investment",
				"username",
				"artifact"
			],
			"description": "<add description>",
			"ident": 30
		},
		"operates_in": {
			"range": [
				"sector"
			],
			"name": "operates_in",
			"domain": [
				"person",
				"company"
			],
			"description": "<add description>",
			"ident": 40
		},
		"endorses": {
			"range": [
				"person"
			],
			"name": "endorses",
			"domain": [
				"reference"
			],
			"description": "<add description>",
			"ident": 42
		},
		"possesses": {
			"range": [
				"skill"
			],
			"name": "possesses",
			"domain": [
				"person",
				"company",
				"username"
			],
			"description": "<add description>",
			"ident": 39
		},
		"invests_in": {
			"range": [
				"company"
			],
			"name": "invests_in",
			"domain": [
				"person",
				"company"
			],
			"description": "<add description>",
			"ident": 43
		},
		"directs": {
			"range": [
				"company"
			],
			"name": "directs",
			"domain": [
				"person"
			],
			"description": "<add description>",
			"ident": 48
		},
		"is_located_in": {
			"range": [
				"current_location",
				"location"
			],
			"name": "is_located_in",
			"domain": [
				"person",
				"company",
				"username"
			],
			"description": "<add description>",
			"ident": 32
		},
		"obtained_from": {
			"range": [
				"academy"
			],
			"name": "obtained_from",
			"domain": [
				"degree"
			],
			"description": "<add description>",
			"ident": 38
		},
		"has_url": {
			"range": [
				"linkedin",
				"twitter",
				"homepage",
				"facebook",
				"stackoverflow",
				"github",
				"rss",
				"aboutme",
				"project"
			],
			"name": "has_url",
			"domain": [
				"person",
				"company",
				"username",
				"artifact"
			],
			"description": "<add description>",
			"ident": 33
		},
		"has_role": {
			"range": [
				"current_role"
			],
			"name": "has_role",
			"domain": [
				"person"
			],
			"description": "<add description>",
			"ident": 31
		},
		"works_at": {
			"range": [
				"company",
				"current_company"
			],
			"name": "works_at",
			"domain": [
				"person",
				"username"
			],
			"description": "<add description>",
			"ident": 46
		},
		"studied_at": {
			"range": [
				"academy"
			],
			"name": "studied_at",
			"domain": [
				"person"
			],
			"description": "<add description>",
			"ident": 36
		},
		"has_degree": {
			"range": [
				"degree"
			],
			"name": "has_degree",
			"domain": [
				"person"
			],
			"description": "<add description>",
			"ident": 37
		},
		"assists": {
			"range": [
				"company"
			],
			"name": "assists",
			"domain": [
				"company"
			],
			"description": "<add description>",
			"ident": 45
		},
		"founded": {
			"range": [
				"company"
			],
			"name": "founded",
			"domain": [
				"person"
			],
			"description": "<add description>",
			"ident": 44
		},
		"with_role": {
			"range": [
				"role"
			],
			"name": "with_role",
			"domain": [
				"company"
			],
			"description": "<add description>",
			"ident": 34
		}
	},
	"concepts": {
		"time_frame": {
			"labels": [
				"ENTITY"
			],
			"name": "time_frame",
			"description": "<add description>",
			"ident": 24
		},
		"type": {
			"labels": [
				"ENTITY"
			],
			"name": "type",
			"description": "<add description>",
			"ident": 28
		},
		"github": {
			"labels": [
				"ENTITY",
				"URL",
				"SOCIAL",
				"GITHUB"
			],
			"name": "github",
			"description": "<add description>",
			"ident": 10
		},
		"degree": {
			"labels": [
				"ENTITY"
			],
			"name": "degree",
			"description": "<add description>",
			"ident": 7
		},
		"homepage": {
			"labels": [
				"ENTITY",
				"URL",
				"SOCIAL",
				"HOMEPAGE"
			],
			"name": "homepage",
			"description": "<add description>",
			"ident": 11
		},
		"tagline": {
			"labels": [
				"ENTITY"
			],
			"name": "tagline",
			"description": "<add description>",
			"ident": 21
		},
		"achievements": {
			"labels": [
				"ENTITY"
			],
			"name": "achievements",
			"description": "<add description>",
			"ident": 3
		},
		"company": {
			"labels": [
				"ENTITY",
				"COMPANY"
			],
			"name": "company",
			"description": "<add description>",
			"ident": 4
		},
		"sector": {
			"labels": [
				"ENTITY",
				"SECTOR"
			],
			"name": "sector",
			"description": "<add description>",
			"ident": 19
		},
		"current_company": {
			"labels": [
				"ENTITY"
			],
			"name": "current_company",
			"description": "<add description>",
			"ident": 51
		},
		"facebook": {
			"labels": [
				"ENTITY",
				"URL",
				"SOCIAL",
				"FACEBOOK"
			],
			"name": "facebook",
			"description": "<add description>",
			"ident": 9
		},
		"username": {
			"labels": [
				"ENTITY"
			],
			"name": "username",
			"description": "<add description>",
			"ident": 50
		},
		"size": {
			"labels": [
				"ENTITY"
			],
			"name": "size",
			"description": "<add description>",
			"ident": 26
		},
		"aboutme": {
			"labels": [
				"ENTITY",
				"URL",
				"SOCIAL",
				"ABOUTME"
			],
			"name": "aboutme",
			"description": "<add description>",
			"ident": 29
		},
		"reference": {
			"labels": [
				"ENTITY",
				"REFERENCE"
			],
			"name": "reference",
			"description": "<add description>",
			"ident": 17
		},
		"role": {
			"labels": [
				"ENTITY",
				"ROLE"
			],
			"name": "role",
			"description": "<add description>",
			"ident": 18
		},
		"person": {
			"labels": [
				"ENTITY",
				"PERSON"
			],
			"name": "person",
			"description": "<add description>",
			"ident": 1
		},
		"rss": {
			"labels": [
				"ENTITY",
				"URL",
				"SOCIAL",
				"RSS"
			],
			"name": "rss",
			"description": "<add description>",
			"ident": 25
		},
		"stackoverflow": {
			"labels": [
				"ENTITY",
				"URL",
				"SOCIAL",
				"STACKOVERFLOW"
			],
			"name": "stackoverflow",
			"description": "<add description>",
			"ident": 13
		},
		"regex": {
			"labels": [
				"ENTITY"
			],
			"name": "regex",
			"description": "<add description>",
			"ident": 23
		},
		"academy": {
			"labels": [
				"ENTITY",
				"COMPANY",
				"ACADEMY"
			],
			"name": "academy",
			"description": "<add description>",
			"ident": 2
		},
		"twitter": {
			"labels": [
				"ENTITY",
				"URL",
				"SOCIAL",
				"TWITTER"
			],
			"name": "twitter",
			"description": "<add description>",
			"ident": 14
		},
		"mission": {
			"labels": [
				"ENTITY"
			],
			"name": "mission",
			"description": "<add description>",
			"ident": 16
		},
		"current_location": {
			"labels": [
				"ENTITY",
				"LOCATION"
			],
			"name": "current_location",
			"description": "<add description>",
			"ident": 5
		},
		"investment": {
			"labels": [
				"ENTITY",
				"INVESTMENT"
			],
			"name": "investment",
			"description": "<add description>",
			"ident": 27
		},
		"linkedin": {
			"labels": [
				"ENTITY",
				"URL",
				"SOCIAL",
				"LINKEDIN"
			],
			"name": "linkedin",
			"description": "<add description>",
			"ident": 12
		},
		"timestamp": {
			"labels": [
				"ENTITY"
			],
			"name": "timestamp",
			"description": "<add description>",
			"ident": 22
		},
		"skill": {
			"labels": [
				"ENTITY",
				"SKILL"
			],
			"name": "skill",
			"description": "<add description>",
			"ident": 20
		},
		"location": {
			"labels": [
				"ENTITY",
				"LOCATION"
			],
			"name": "location",
			"description": "<add description>",
			"ident": 15
		},
		"artifact": {
			"labels": [
				"ENTITY"
			],
			"name": "artifact",
			"description": "<add description>",
			"ident": 52
		},
		"current_role": {
			"labels": [
				"ENTITY",
				"ROLE"
			],
			"name": "current_role",
			"description": "<add description>",
			"ident": 6
		},
		"project": {
			"labels": [
				"ENTITY",
				"URL",
				"ARTIFACT",
				"PROJECT"
			],
			"name": "project",
			"description": "<add description>",
			"ident": 53
		},
		"description": {
			"labels": [
				"ENTITY"
			],
			"name": "description",
			"description": "<add description>",
			"ident": 8
		}
	}
}