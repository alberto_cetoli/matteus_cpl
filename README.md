CPL pattern extractor (by Matteus Tanha)
========================================

This is an implementation of the work done in the article [
Coupling semi-supervised learning of categories and relations
](http://dl.acm.org/citation.cfm?id=1621829.1621830).

The system can be used upon calling `cpl_extractor.py`